# GPS LocationListener #

En esta version se investigó la implementacion de Localización usando Google Play Services. En las aplicaciones anteriores se utilizaron la version antigua del API(Gps o Red), ahora se utilizara una nueva API llamada **FusedLocationApi**, el cual junto al GoogleApiClient nos da una mejor localización disponible.

**FusedLocationApi** permite indicar para activar las actualizaciones de la localizacion y para detenerlo.

```
#!java

protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }
```

* Para esta presentacion era necesario investigar acerca de **Services** para ejecutar funciones como la actualizacion de GPS, el cual puede funcionar sin necesidad de tener abierta la aplicacion.

**Services**
Es un componente que se ejecuta sin interaccion del usuario.
se define en el  AndroidManifest.xml

```
#!xml

<service android:name=".GpsLocationService" />
```


### Notas ###

* Agregar permisos correspondientes
* Agregar el Meta-Data en Application
* Agregar dependencia "compile 'com.google.android.gms:play-services:6.+'"